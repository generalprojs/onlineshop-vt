import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { logout } from '../../redux/slices/authSlice';
import './NavigationBar.css';

function NavigationBar() {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    return(
        <>
            <div className='nav-list'>
                <a onClick={() => navigate('/home')}>Home</a>
                <a onClick={() => navigate('/profile')}>Profile</a>
                <a onClick={() => navigate('about')}>About me</a>
                <a onClick={() =>  dispatch(logout())}>Log Out</a>
            </div>
        </>
    )
}

export default NavigationBar