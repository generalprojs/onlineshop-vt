import { useDispatch } from 'react-redux';
import { login } from '../../redux/slices/authSlice';
import './LoginComponent.css';

function LoginComponent() {
    const dispatch = useDispatch();
    return(
        <div>
            <h3>Bienvenido. Inicie sesion para empezar</h3>
            <button onClick={() => dispatch(login())}>Log in</button>
        </div>
    );
}

export default LoginComponent;