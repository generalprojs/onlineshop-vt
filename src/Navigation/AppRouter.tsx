import { 
    BrowserRouter as Router, 
    Routes, 
    Route,
    RouteObject,
    Navigate,
    Outlet,
} from 'react-router-dom'
import { NavigationBar, LoginComponent } from '../components'
import { useSelector } from 'react-redux';

const PrivateRoute = ({ isAuthorized, redirectPath = '/' } : { isAuthorized: boolean, redirectPath?: string }) => {
    if(!isAuthorized) {
      return <Navigate replace to={redirectPath} />
    }
    return <Outlet />
  }

// const Login = () => (<><h3>Welcome to login</h3></>)
const HomePage = () => (<><h3>Home</h3></>)
const AboutPage = () => (<><h3>About</h3></>)
const ProfilePage = () => (<><h3>Profile</h3></>)

const routes: RouteObject[] = [
    {
        path: '/home',
        element: <HomePage />
    },
    {
        path: '/about',
        element: <AboutPage />
    },
    {
        path: '/profile',
        element: <ProfilePage />
    }
  ];

function AppRouter() {
    // const isAuthorized = true;
    const isAuthorized = useSelector((state: any) => state.auth.isAuthenticated);
    return(
        <Router>
            {isAuthorized && <NavigationBar />}
            <Routes>
                <Route path='/' element={<LoginComponent />} />
                <Route element={<PrivateRoute  isAuthorized={isAuthorized}/>}>
                    {routes.map(({path, element}) => 
                        <Route key={path} path={path} element={element} />
                    )}
                </Route>
                <Route path='*' element={<Navigate replace to={'/'} />} />
            </Routes>
        </Router>
    );
}

export default AppRouter;