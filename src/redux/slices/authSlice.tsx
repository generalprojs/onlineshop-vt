import { createSlice } from '@reduxjs/toolkit';

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        isAuthenticated: false,
        globalCount: 0
    },
    reducers: {
        login: (state) => {
            state.isAuthenticated = true
        },
        logout: (state) => {
            state.isAuthenticated = false
        },
        increment: (state) =>{
            state.globalCount += 1
        },
        decrement: (state) => {
            state.globalCount -= 1
        }
    }
})

export const { login, logout, increment, decrement } = authSlice.actions;
export default authSlice.reducer;