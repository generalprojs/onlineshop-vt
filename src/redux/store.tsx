import { configureStore, combineReducers } from '@reduxjs/toolkit';
import authReducer  from './slices/authSlice';

const rootReducer = combineReducers({
    // Add reducers here
    auth: authReducer
})

const store = configureStore({
    reducer: rootReducer
})

export default store;